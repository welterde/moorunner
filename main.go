package main

import (
	"log"
	"flag"
	"os"
	"context"
	"os/exec"
	"os/signal"
	"syscall"
	"strconv"
	"bufio"
	"fmt"
	"regexp"
	"time"
	"errors"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

func check(e error) {
	if e != nil {
		log.Fatalln(e)
	}
}

var finishedSnapshotRE = regexp.MustCompile(`CHECKPOINTING on .+ finished`)

func runMOO(mooPath string, mooPort int, prependTime bool, dbIn string, dbOut string, mooSnapshoted chan<- bool, mooTerminated chan<- bool, terminating <-chan bool) {
	cmd := exec.Command(mooPath, dbIn, dbOut, "-p", strconv.Itoa(mooPort))
	stdout, err := cmd.StderrPipe()
	check(err)
	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}
	rdr := bufio.NewScanner(stdout)

	go func() {
		<- terminating
		cmd.Process.Signal(os.Interrupt)
	}()

	// read output until exit
	for rdr.Scan() {
		line := rdr.Text()
		if finishedSnapshotRE.MatchString(line) {
			mooSnapshoted <- true
		}
		if prependTime {
			log.Printf("%s\n", line)
		} else {
			fmt.Printf("%s\n", line)
		}
	}
	// MOO has terminated
	if err := rdr.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
	cmd.Wait()
	mooTerminated <- true
}

func genDayTag() string {
	now := time.Now()
	return now.Format(time.RFC3339)
}

func uploadDatabase(client *minio.Client, bucketName string, dbFile string, objName string) error {
	log.Printf("Uploading db-file %s as %s to bucket %s\n", dbFile, objName, bucketName)
	info, err := client.FPutObject(context.Background(), bucketName, objName, dbFile, minio.PutObjectOptions{})
	if err != nil {
		return err
	}
	log.Printf("Successfully uploaded %s of size %d\n", objName, info.Size)
	return nil
}

var dbIn = "old.db"
var dbOut = "new.db"

func main() {
	endpoint := flag.String("s3_endpoint", "localhost:8008", "S3 Endpoint")
	accessKeyID := flag.String("s3_accesskey", "", "S3 Access KeyID")
	secretAccessKey := flag.String("s3_secret", "", "S3 Secret Access Key")
	useSSL := flag.Bool("s3_ssl", true, "S3 Use SSL")
	bucketName := flag.String("s3_bucket", "moo", "S3 Bucket to use")
	mooPort := flag.Int("moo_port", 1234, "Port MOO should listen on")
	mooBinary := flag.String("moo_binary", "lambdamoo", "MOO Binary we should launch")
	mooPrependTime := flag.Bool("moo_prepend_time", true, "Prepend the time to the MOO output")
	
	flag.Parse()
	log.SetOutput(os.Stdout)
	
	ctx := context.Background()

	// Initialize minio client object.
	minioClient, err := minio.New(*endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(*accessKeyID, *secretAccessKey, ""),
		Secure: *useSSL,
	})
	if err != nil {
		log.Fatalln(err)
	}

	// ensure the bucket exists
	exists, errBucketExists := minioClient.BucketExists(ctx, *bucketName)
	if errBucketExists != nil {
		log.Fatalln(err)
	}
	if !exists {
		log.Fatalln("Bucket does not exist!")
	}

	// if previous database exists upload it
	if _, err := os.Stat(dbIn); err == nil {
		err = uploadDatabase(minioClient, *bucketName, dbIn, fmt.Sprintf("backup/old/%s", genDayTag()))
		check(err)
	}

	// if new database exists upload it
	if _, err := os.Stat(dbOut); err == nil {
		objName := fmt.Sprintf("backup/new/%s.db", genDayTag())
		err = uploadDatabase(minioClient, *bucketName, dbOut, objName)
		check(err)
		// move it to location of old database
		os.Rename(dbOut, dbIn)
		// make it the current database in the S3 repo
		src := minio.CopySrcOptions{
			Bucket: *bucketName,
			Object: objName,
		}
		dst := minio.CopyDestOptions{
			Bucket: *bucketName,
			Object: "current.db",
		}
		info, err := minioClient.CopyObject(ctx, dst, src)
		check(err)
		log.Printf("Successfully copied over object: %v\n", info)
	}

	_, err = os.Stat(dbIn)
	if errors.Is(err, os.ErrNotExist) {
		// we still have no input database.. download it from S3
		err = minioClient.FGetObject(ctx, *bucketName, "current.db", dbIn, minio.GetObjectOptions{})
		check(err)
	} else {
		// does not exist or something else is wrong with it
		check(err)
	}

	// setup signal intercept
	sigs := make(chan os.Signal, 1)
	terminating := make(chan bool, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func(){
		sig := <- sigs
		fmt.Printf("Received signal %v. Starting shutdown sequence..\n", sig)
		terminating <- true
	}()

	// setup channels with MOO runner routine
	mooSnapshoted := make(chan bool, 1)
	mooTerminated := make(chan bool, 1)

	go runMOO(*mooBinary, *mooPort, *mooPrependTime, dbIn, dbOut, mooSnapshoted, mooTerminated, terminating)

	terminated := false
	for !terminated {
		select {
		case <- mooSnapshoted:
			// lambdamoo prints the finished snapshotting message before renaming the file into place
			time.Sleep(1 * time.Second)
		case <- mooTerminated:
			terminated = true
		}
		objName := fmt.Sprintf("backup/cur/%s.db", genDayTag())
		err = uploadDatabase(minioClient, *bucketName, dbOut, objName)
		if err != nil {
			log.Printf("Error uploading current snapshot: %v\n", err)
		}
	}
}

